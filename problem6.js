//    Create a function to retrieve and display the first hobby of each individual in the dataset.


function displayFirstHobbie(arrayOfObjects) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;
        let firstHobbieList = [];

        for (let index = 0; index < size; index++) {
            firstHobbieList.push(arrayOfObjects[index]['hobbies'][0])
        }

        return firstHobbieList;
    } else {
        return []
    }

};


module.exports = displayFirstHobbie