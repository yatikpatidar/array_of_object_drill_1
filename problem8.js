//    Implement a loop to access and log the city and country of each individual in the dataset.



function locationDetailOfAll(arrayOfObjects) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;
        let locationList = [];

        for (let index = 0; index < size; index++) {
            let detailList = { city: "", country: "" }

            detailList['city'] = arrayOfObjects[index]['city']
            detailList['country'] = arrayOfObjects[index]['country']
            locationList.push(detailList)

        }

        return locationList
    } else {
        return []
    }

};


module.exports = locationDetailOfAll