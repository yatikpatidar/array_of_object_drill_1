 //    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.
  

 const arrayOfObjects = require('../data');

 const displayNameOfStudents = require('../problem3');
 
 const studentNameList = displayNameOfStudents(arrayOfObjects, "Australia");
 
 if (studentNameList.length != 0) {
     console.log("displaying the names of individuals who are students and live in Australia." ,studentNameList)
 } else {
     console.log("please once check before passing objects as argument ")
 }