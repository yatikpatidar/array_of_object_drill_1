//    Create a function to retrieve and display the first hobby of each individual in the dataset.


const arrayOfObjects = require('../data');

const printFirstHobbie = require('../problem6');

const firstHobbieList = printFirstHobbie(arrayOfObjects);

if (firstHobbieList.length != 0) {
    console.log("printing first hobbie ", firstHobbieList)
} else {
    console.log("please once check before passing objects as argument ")
}