//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.


const arrayOfObjects = require('../data');

const retrivingHobbies = require('../problem2');

const hobbiesList = retrivingHobbies(arrayOfObjects, 30);

if (hobbiesList.length != 0) {
    console.log("printing the hobbies of individuals with a specific age ", hobbiesList)
} else {
    console.log("please once check before passing objects as argument ")
}