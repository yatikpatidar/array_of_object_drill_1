//    Implement a loop to access and print the ages of all individuals in the dataset.



const arrayOfObjects = require('../data');

const printAgeOfAll = require('../problem5');

const ageList = printAgeOfAll(arrayOfObjects);

if (ageList.length != 0) {
    console.log("printing age of all ", ageList)
} else {
    console.log("please once check before passing objects as argument ")
}