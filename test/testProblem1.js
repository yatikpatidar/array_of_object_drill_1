//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.


const arrayOfObjects = require('../data') ;

const getEmailsAddress = require('../problem1') ;

const emailList = getEmailsAddress(arrayOfObjects) ;

if(emailList.length != 0){
    console.log("email address of all individuals ",emailList)
}else{
    console.log("please once check before passing objects as argument ")
}