//    Write a function that accesses and prints the names and email addresses of individuals aged 25.


const arrayOfObjects = require('../data');


const personalDetailAtSpecificAge = require('../problem7');

const personalDetailList = personalDetailAtSpecificAge(arrayOfObjects, 30);

if (personalDetailList.length != 0) {
    console.log("printing personal detail at specific age", personalDetailList)
} else {
    console.log("please once check before passing objects as argument ")
}