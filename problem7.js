//    Write a function that accesses and prints the names and email addresses of individuals aged 25.



function personalDetailAtSpecificAge(arrayOfObjects, age) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;
        let hobbiesList = [];

        for (let index = 0; index < size; index++) {
            let detailList = { name: "", email: "" }

            if (arrayOfObjects[index]['age'] === age) {

                detailList['name'] = arrayOfObjects[index]['name']
                detailList['email'] = arrayOfObjects[index]['email']
                hobbiesList.push(detailList)
            }
        }
        return hobbiesList;
    } else {
        return [];
    }

};


module.exports = personalDetailAtSpecificAge;