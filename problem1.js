//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.

function getEmailsAddress(arrayOfObjects) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;

        let email_var = [];

        for (let index = 0; index < size; index++) {
            email_var.push(arrayOfObjects[index]['email']);
        }

        return email_var;

    } else {
        return [];
    }

};


module.exports = getEmailsAddress;