//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.



function displayNameOfStudents(arrayOfObjects, country) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;

        let studentNameList = [];
        for (let index = 0; index < size; index++) {
            if (arrayOfObjects[index]['isStudent'] && arrayOfObjects[index]['country'] === country) {
                studentNameList.push(arrayOfObjects[index]['name'])
            }
        }

        return studentNameList;
        
    } else {
        return [];
    }

};


module.exports = displayNameOfStudents;