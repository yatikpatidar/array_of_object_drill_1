//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.



function studentDetailAtParticularIndex(arrayOfObjects, givenIndex) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0) && (givenIndex >= 0 && givenIndex < arrayOfObjects.length)) {

        let result = `student name is ' ${arrayOfObjects[givenIndex]['name']} ' and city is ' ${arrayOfObjects[givenIndex]['city']} '`

        return result;
    } else {
        return "";
    }

};


module.exports = studentDetailAtParticularIndex;