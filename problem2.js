//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.


function retrivingHobbies(arrayOfObjects, age) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;

        let hobbiesList = [];
        for (let index = 0; index < size; index++) {
            if (arrayOfObjects[index]['age'] === age) {
                hobbiesList.push(arrayOfObjects[index]['hobbies']);
            }
        }
        return hobbiesList;
    } else {
        return [];
    }

};


module.exports = retrivingHobbies;