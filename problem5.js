//    Implement a loop to access and print the ages of all individuals in the dataset.


function printAgeOfAll(arrayOfObjects) {

    if (Array.isArray(arrayOfObjects) && (arrayOfObjects.length > 0)) {

        const size = arrayOfObjects.length;
        let ageList = [];

        for (let index = 0; index < size; index++) {
            ageList.push(arrayOfObjects[index]['age'])
        }

        return ageList;
    } else {
        return []
    }

};


module.exports = printAgeOfAll